---
**** Welcome to PG_for_RHEL-DOCKER ****

 This reposititory's for locally deploying Prometheus container
  with Grafana dashboard to developers' laptop.
 
 The repository has one-stop scenario to deploy the following resources.
 
 - A bridge network dedicated to Prometheus-Grafana ovservability solution
 - A Prometheus container
 - A Prometheus node-exporter container
 - A Grafana container
 
  ************************************************************
  *********        Version of each product           *********
  ***               Prometheus:    v2.34.0                 ***
  ***               Node-exporter: v1.3.1                  ***
  ***               Grafana:       8.4.4                   ***
  ************************************************************

## Pre-requisites
 Developers' laptop where the solution will be installed must be RHEL,
  or CentOS 7.x - 8.x based.
 Then its containarisation engine should be Docker.

## Installation for Docker-compose command user 
 If you are docker-compose user, 
  it will be the easiest way to deploy the entire solution
  by the following way.
  
 1. Download this repository as a zip file on your laptop
 2. Decompress the zip file directly under your home directory.
    (It must be the same as $HOME)
 3. Rename the root directory to 'PG_FOR_RHEL-DOCKER'
 
 4. Run docker-compose command with arguments below:
    docker-compose run -f docker-compose.yml prometheus node-exporter grafana

## Installation for Docker-compose command user 
 If you don't have docker-compose or need to deploy
  the entire solution temporarily,
  please follow this way.
  
 1. Download this repository as a zip file on your laptop
 2. Decompress the zip file directly under anyware you want.
    (It isn't necesarily under your $HOME dir)
 3. Change the permission of deploy.sh file.
    chmod 0755 deploy.sh
 4. Run deploy.sh shellscript without any argument.
    ./deploy.sh

 (when you need to remove the entire solution from your laptop)
 
 3. Change the permission of delete.sh file.
    chmod 0755 delete.sh
 4. Run delete.sh shellscript without any argument.
    ./delete.sh