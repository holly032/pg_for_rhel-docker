#!/bin/bash

set -e
## set local variables
mon_work_dir=`pwd`
mon_conf_dir=${mon_work_dir}/files/
mon_nw_dockerhost='172.29.0.33'
mon_nw_sub='172.29.0.32/28'
mon_nw_name='monitor'

## define its subnet of bridge network for monitoring
echo '*** Prior to creating bridge network "monitor", kindly confirm its subnet.'
echo '*** We are going to create a bridge network with "172.29.0.32/28".'
echo '*** The netmask is fixed (/28), but if you have a desired subnet like "192.168.65.0/28"'
echo "    please specify in the following confirmation"

read -p 'Would you confirm its subnet of a bridge network "monitor"? (y/N): ' cfm

if [ $cfm != 'y' ]
then
  read -p 'Please enter your desired subnet without netmask e.g.) 192.168.65.0: ' sub
  mon_nw_sub="${sub}/28"
  
  seg=`echo ${sub} | grep -oP "\d+"`
  last_seg=`echo ${seg} | awk '{print $4}'`
  let "last_seg+=1"
  mon_nw_dockerhost=`echo ${sub} | grep -oP "^\d+.\d+.\d+."`
  mon_nw_dockerhost=`echo ${mon_nw_dockerhost}${last_seg}`
fi

sed s/DOCKERHOST/"${mon_nw_dockerhost}"/g \
  ${mon_conf_dir}prometheus/prometheus.template.yml > \
  ${mon_conf_dir}prometheus/prometheus.yml

sed s/DOCKERHOST/"${mon_nw_dockerhost}"/g \
  ${mon_conf_dir}grafana/provisioning/datasources/datasource.template.yml > \
  ${mon_conf_dir}grafana/provisioning/datasources/datasource.yml

## create bridge network for monitoring
echo "Creating a bridge network (${mon_nw_sub})......"
docker network create -d bridge \
  --attachable --subnet $mon_nw_sub \
  $mon_nw_name >/dev/null 2>&1
echo "  ......done!"

## run container for node-exporter
echo "Creating a container 'node-exporter'......"
docker run -d -p 9100:9100 --name node-exporter \
  --restart=always --pid="host" \
  --network=$mon_nw_name \
  prom/node-exporter:v1.3.1 >/dev/null 2>&1
echo "  ......done!"

## run container for Prometheus
echo "Creating a container 'Prometheus'......"
docker run -d -p 9090:9090 --name=prometheus \
  --restart=always \
  -v ${mon_conf_dir}prometheus/prometheus.yml:/etc/prometheus/prometheus.yml \
  --network=$mon_nw_name \
  prom/prometheus:v2.34.0 \
  --storage.tsdb.retention.time=1h \
  --config.file=/etc/prometheus/prometheus.yml >/dev/null 2>&1
echo "  ......done!"

## run container for Grafana
echo "Creating a container 'Grafana'......"
docker run -d -p 3000:3000 --name=grafana \
  --restart=always \
  -v ${mon_conf_dir}grafana/provisioning/datasources/datasource.yml:/etc/grafana/provisioning/datasources/datasource.yml \
  --network=$mon_nw_name \
  grafana/grafana:8.4.4 >/dev/null 2>&1
echo "  ......done!"
