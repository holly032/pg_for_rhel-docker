#!/bin/bash

set -e
## set local variables
mon_work_dir=`pwd`
mon_conf_dir=${mon_work_dir}/files/
mon_nw_name='monitor'
mon_nw_id='0'
mon_node_id=0

## remove container for Grafana
echo "deleting a  a container 'Grafana'......"
mon_node_id=`docker ps -f status=running | awk '/grafana/ {print $1}'`
if [ $mon_node_id != '0' ]
then
  docker kill $mon_node_id >/dev/null 2>&1
fi
docker rm $mon_node_id >/dev/null 2>&1
mon_node_id=0
rm -f ${mon_conf_dir}grafana/provisioning/datasources/datasource.yml
echo "  ......done!"

## remove container for Prometheus
echo "deleting a  a container 'Prometheus'......"
mon_node_id=`docker ps -f status=running | awk '/prometheus/ {print $1}'`
if [ $mon_node_id != '0' ]
then
  docker kill $mon_node_id >/dev/null 2>&1
fi
docker rm $mon_node_id >/dev/null 2>&1
mon_node_id=0
rm -f ${mon_conf_dir}prometheus/prometheus.yml
echo "  ......done!"

## remove container for node-exporter
echo "deleting a  a container 'node-exporter'......"
mon_node_id=`docker ps -f status=running | awk '/node-exporter/ {print $1}'`
if [ $mon_node_id != '0' ]
then
  docker kill $mon_node_id >/dev/null 2>&1
fi
docker rm $mon_node_id >/dev/null 2>&1
mon_node_id=0
echo "  ......done!"

## remove bridge network for monitoring
echo "Creating a bridge network 'monitor'......"
mon_nw_id=`docker network ls | awk '/monitor/ {print $1}'`
if [ $mon_nw_id != '0' ]
then
  docker network rm $mon_nw_id >/dev/null 2>&1
fi
mon_nw_id=0
echo "  ......done!"
